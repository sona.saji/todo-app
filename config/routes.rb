Rails.application.routes.draw do
  resources :tasks do
    patch :change_status, on: :member
  end
  root "tasks#index"
end
