# frozen_string_literal: true

require 'rails_helper'

RSpec.describe(Task, type: :model) do
  # create task
  let(:task) { create :task }

  describe 'create task' do
    it 'new task can be added' do
      expect(task).to(be_valid)
      expect(described_class.last.name).to(eq(task.name))
    end
  end

  # validates task model
  context 'task model validation' do
    # task name validation
    it 'must have an task name' do
      task = described_class.new(name: '').save
      expect(task).to(eq(false))
    end

    # validates status
    it 'must have a status' do
      task = described_class.new(status: '').save
      expect(task).to(eq(false))
    end
  end
end
