require 'rails_helper'

RSpec.describe('tasks', type: :request) do
  
  let(:task) { create(:task)}

  describe 'GET /index' do
    # let(:task) { create(:task)}
    it 'renders a successful response' do
      get tasks_path
      expect(response).to(have_http_status(:success))
    end
  end
  describe 'task creation' do
    it 'new task can be created' do
      expect do
        post(tasks_path, params: {
          task: {
            name: 'Micro',
            status: "incomplete",
          }
        }
        )
      end.to(change(Task, :count).by(1))
    end
  end

  describe 'update task' do
    let!(:task) { create(:task)}
    it 'updates the requested task' do
      patch task_path(task, params: {
        task: {
          name: 'Test_name',
          status: ''
        }
      })
    end
  end

  describe 'DELETE /destroy task' do
    let!(:task) { create(:task) }

    it 'destroys the requested task' do
      expect do
        delete(task_path(task))
      end.to(change(Task, :count).by(-1))
    end

    it 'redirects to the organisations list' do
      delete task_url(task)
      expect(response).to(redirect_to(tasks_url))
    end
  end

  describe 'update task status' do
    let!(:task) { create(:task)}
    it 'updates the requested task status' do
      patch change_status_task_path(task, params: {
        task: {
          status: 'complete'
        }
      })
    end
  end
end